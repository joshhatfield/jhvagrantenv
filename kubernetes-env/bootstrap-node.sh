#!/bin/sh

# Run on VM to bootstrap Puppet Agent nodes

    echo "" | sudo tee --append /etc/hosts 2> /dev/null && \
    echo "# Host config for kube" | sudo tee --append /etc/hosts 2> /dev/null && \
    echo "192.168.32.10   kmaster1.example.com  kmaster1" | sudo tee --append /etc/hosts 2> /dev/null && \
    echo "192.168.32.20   kmaster2.example.com  kmaster2" | sudo tee --append /etc/hosts 2> /dev/null && \
    echo "192.168.32.30   knode1.example.com  knode1" | sudo tee --append /etc/hosts 2> /dev/null && \
    echo "192.168.32.40   knode2.example.com  knode2" | sudo tee --append /etc/hosts 2> /dev/null

     # Basic bootstrap
     sudo apt-get update
     sudo apt-get -y install git wget

     # -----------------Kubernetes environment

     # docker install
     sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
     sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
     sudo add-apt-repository "deb https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"
     sudo apt-get update && sudo apt-get install -y docker-ce=$(apt-cache madison docker-ce | grep 17.03 | head -1 | awk '{print $3}')

     # Kubeutils install
     sudo apt-get update && sudo apt-get install -y apt-transport-https curl
     sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

sudo tee /etc/apt/sources.list.d/kubernetes.list <<EOF
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
     sudo apt-get update
     sudo apt-get install -y kubelet kubeadm kubectl


     exit 0

